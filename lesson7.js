const express = require('express');
const app = express();
const user = require('./routes/user');
const admin = require('./routes/admin');

app.use("/client", user, admin);
app.listen(3000);
