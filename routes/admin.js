const express = require('express');
const administrator = express.Router();
const jsonParser = express.json();

global.admin = {};

administrator.post("/admin/create", jsonParser, function(request, response){
    global.admin.id = request.body.id;
    global.admin.adminName = request.body.name; 
    response.send(`<h3>Данные отправлены. Admin id: ${global.admin.id}, Admin name: ${global.admin.adminName}</h3>`);    
});

administrator.get("/admin/:id", function(request, response){
    response.send(`<h3>Данные получены: Admin id: ${global.admin.id}, Admin name: ${global.admin.adminName}</h3>`);    
});

administrator.delete("/admin/:id", function(request, response){
    delete global.admin.id;
    delete global.admin.adminName;
    response.send("<h3>Данные удалены</h3>");    
});

administrator.put("/admin/:id", jsonParser, function(request, response){
    global.admin.id = request.body.id;
    global.admin.adminName = request.body.name;
    response.send(`<h3>Данные обновлены: Admin id: ${global.admin.id}, Admin name: ${global.admin.adminName}</h3>`);    
});

module.exports = administrator;
