const express = require('express');
const user1 = express.Router();
const urlencodedParser = express.urlencoded({extended: true});

global.user = {};

user1.post("/user/create", urlencodedParser, function(request, response){
    global.user.id = request.body.id;
    global.user.userName = request.body.userName; 
    response.send(`<h3>Данные отправлены. User id: ${global.user.id}, User name: ${global.user.userName}</h3>`);  
});

user1.get("/user/:id", function(request, response){
    response.send(`<h3>Данные получены: User id: ${global.user.id}, User name: ${global.user.userName}</h3>`);    
});

user1.delete("/user/:id", function(request, response){
    delete global.user.id;
    delete global.user.userName;
    response.send("<h3>Данные удалены</h3>");   
});

user1.put("/user/:id", urlencodedParser, function(request, response){
    global.user.id = request.body.id;
    global.user.userName = request.body.userName; 
    response.send(`<h3>Данные обновлены: User id: ${global.user.id}, User name: ${global.user.userName}</h3>`);   
});

module.exports = user1;
